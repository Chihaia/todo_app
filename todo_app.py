import pymysql
import warnings
# connect to mysql server and creating the "todo_app" database
db = pymysql.connect("127.0.0.1", "root", "test1234", "")
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    with db.cursor() as c:
        # for a new and empty database use line 9
        # c.execute("drop schema if exists `todo_app`;")
        c.execute("CREATE SCHEMA IF NOT EXISTS `todo_app` DEFAULT CHARACTER SET UTF8MB4;")
    db.close()
# checking if the "todo_app" database was created with a new connection attempt
db = pymysql.connect("127.0.0.1", "root", "test1234", "todo_app")
# declaring variables at the top-level of module for open and closed tasks ids
closed_id_list = []
open_id_list = []
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    # creating the table if the table not exists
    with db.cursor() as c:
        c.execute("create table if not exists `tasks` (`id` int not null primary key auto_increment, "
                  "task text not null, done boolean);")

        # declaring a function to display the open tasks
        def show_open_tasks():
            with db.cursor() as c:
                c.execute("select id, task, done from `tasks` where not done;")
                for row in c.fetchall():
                    x, y, z = row
                    print(f"ID:{x}: Description:{y}: Done:{z}")

        # declaring a function to display the closed tasks
        def show_closed_tasks():
            with db.cursor() as c:
                c.execute("select id, task, done from `tasks` where done;")
                for row in c.fetchall():
                    x, y, z = row
                    print(f"ID:{x}: Description:{y}: Done:{z}")

        # declaring a function to display the entire task lists
        def show_all_tasks():
            c.execute("select id, task, done from `tasks`;")
            for row in c.fetchall():
                x, y, z = row
                print(f"ID:{x}: Description:{y}: Done:{z}")

        # declaring a function to add a task
        def add_task():
            description = input("Task description: ")
            if description == "":
                print("Cannot insert an empty description, please try again")
                return
            with db.cursor() as c:
                c.execute("insert into `tasks` (task, done) values (%s, FALSE)", (description))
                open_tasks_add()
                db.commit()

        # declaring a function to modify a task from open to closed
        def close_task():
            open_tasks_add()
            if open_id_list:
                while True:
                    try:
                        question_id = int(input
                                ("Please select your task id to close it from the open tasks above: "))
                        break
                    except:
                        print("The id number is not correct, try again")
                if question_id in open_id_list:
                    with db.cursor() as c:
                        c.execute("update `tasks` set done=TRUE where id=%s", (question_id))
                        print(f"You've closed the taks id: {question_id}")
                        open_id_list.remove(question_id)
                        closed_id_list.append(question_id)
                        db.commit()
                else:
                    print("The id number is not correct, try again")
                    close_task()
            else:
                print("There is no opened task to close, please select another option")

        # declaring a function to modify a task from closed to open
        def open_task():
            closed_tasks_add()
            if closed_id_list:
                while True:
                    try:
                        question_id = int(input(
                            "Please select your task id from above to open back from the close tasks:"))
                        break
                    except:
                        print("The id number is not correct, try again")
                if question_id in closed_id_list:
                    with db.cursor() as c:
                        c.execute("update `tasks` set done=FALSE where id=%s", (question_id))
                        print(f"You've opened the taks id: {question_id}")
                        closed_id_list.remove(question_id)
                        open_id_list.append(question_id)
                        db.commit()
                else:
                    print("The id number is not correct, try again")
                    open_task()
            else:
                print("There is no closed task to open back, please select another option")

        # check if the table has values
        def empty_table():
            results = c.execute("""SELECT * from `tasks` limit 1""")
            if not results:
                print("This table is empty!")
        # check if the table has open tasks

        def empty_open_tasks():
            results = c.execute("""SELECT * from `tasks` where `done` = False limit 1;""")
            if not results:
                print("There is no open tasks in the list!")

        # check if the table has closed tasks
        def empty_closed_tasks():
            results = c.execute("""SELECT * from `tasks` where `done` = True limit 1;""")
            if not results:
                print("There is no closed tasks in the list!")

        # add in a list the ids of closed tasks
        def closed_tasks_add():
            with db.cursor() as c:
                c.execute("select id from tasks where done = True;")
                global closed_id_list
                closed_id_list = []
                new_list = []
                closed_id_list.append([list(record) for record in c.fetchall()])
                for x in closed_id_list:
                    for y in x:
                        for z in y:
                            new_list.append(z)
                closed_id_list = new_list

        # add in a list the ids of open tasks
        def open_tasks_add():
            with db.cursor() as c:
                c.execute("select id from tasks where done = False;")
                global open_id_list
                open_id_list = []
                new_list = []
                open_id_list.append([list(record) for record in c.fetchall()])
                for x in open_id_list:
                    for y in x:
                        for z in y:
                            new_list.append(z)
                open_id_list = new_list

        def remove_closed_tasks():
            if closed_id_list:
                while True:
                    try:
                        question_id = int(input(
                            "Please select your task id from above list to delete it: "))
                        break
                    except:
                        print("The id number is not correct, try again")
                if question_id in closed_id_list:
                    with db.cursor() as c:
                        c.execute("delete from `tasks` where id=%s", (question_id))
                        print(f"You've deleted the task id: {question_id}")
                        closed_id_list.remove(question_id)
                        db.commit()
                else:
                    print("The id number is not correct, try again")
                    remove_closed_tasks()
            else:
                print("There is no closed task to delete, please select another option")

        # declaring the variable with the required choices
        task_options = ["show", "modify", "add", "exit", "show all", "delete"]
        # load the open and closed tasks from last db connection
        open_tasks_add()
        closed_tasks_add()
        while task_options:
            questions = input("1.For displaying the open task list please insert the string 'show'\n"
                              "2.For displaying all the tasks please insert the string 'show all'\n"
                              "3.For changing the task status please insert the string 'modify'\n"
                              "4.To add a new task please insert the string 'add'\n"
                              "5.To delete a closed task please insert the string 'delete'\n"
                              "6.To exist the app please insert the string 'exit'\n"
                              "YOUR OPTION IS: ")
            # exit the program
            if questions == 'exit':
                break
            # add a task in the database
            elif questions == 'add':
                add_task()
                print("You have successfully added a task! \n"
                      "What is your next choice?\n"
                      "------------------------")
                while True:
                    print("Do you want to add a new task or you want to return to the MENU?\n 'yes'/'no' or press "
                          "any key to go back in the menu")
                    answer = input()
                    if answer == 'yes':
                        add_task()
                    else:
                        break
            # change the status of a task
            elif questions == 'modify':
                question2 = input("What kind of task do you want to modify?\n"
                                  "Insert 'open' for opened tasks or 'close' for closed tasks: ")
                # modify to close an open task
                if question2 == 'open' and open_id_list:
                    show_open_tasks()
                    close_task()
                    if closed_id_list:
                        while True:
                            print("Do you want to close another task or you want to return to the MENU?\n "
                                  "'yes'/'no' or press any key to go back in the menu")
                            answer = input()
                            if answer == 'yes' and open_id_list:
                                show_open_tasks()
                                close_task()
                            else:
                                empty_open_tasks()
                                break
                    else:
                        empty_open_tasks()
                        continue

                # modify to open back the closed task
                elif question2 == 'close' and closed_id_list:
                    show_closed_tasks()
                    open_task()
                    if closed_id_list:
                        while True:
                            print("Do you want to open a new task or you want to return to the MENU?\n "
                                  "'yes'/'no' or press any key to go back in the menu")
                            answer = input()
                            if answer == 'yes' and closed_id_list:
                                show_closed_tasks()
                                open_task()
                            else:
                                empty_closed_tasks()
                                break
                    else:
                        empty_closed_tasks()
                        continue
                else:
                    empty_closed_tasks()
                    continue

            # delete a task from close tasks
            elif questions == 'delete':
                show_closed_tasks()
                remove_closed_tasks()
                if closed_id_list:
                    print("Do you want to delete another closed task or you want to return to the MENU?\n "
                          "'yes'/'no' or press any key to go back in the menu")
                    answer = input()
                    if answer == 'yes' and closed_id_list:
                        show_closed_tasks()
                        remove_closed_tasks()
                    else:
                        empty_closed_tasks()
                        continue
                else:
                    empty_closed_tasks()
                    continue

            # show only the open tasks
            elif questions == 'show':
                print("Your open tasks are:\n"
                      "---------------------")
                show_open_tasks()
                empty_open_tasks()
                print("---------------------\n"
                      "\tWhat is your next choice?")
            # show all the tasks
            elif questions == 'show all':
                print("Your entire list of tasks are:\n"
                      "---------------------")
                show_all_tasks()
                empty_table()
                print("---------------------\n"
                      "\tWhat is your next choice?")
            else:
                print("Your option is not valid please select again:")
        db.close()
